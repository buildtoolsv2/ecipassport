<?php

namespace App\Models;

use App\Traits\FullSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uniquesession extends Model
{
    use HasFactory;
    use FullSearch;

    const PAGINATE_LIST = [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100];
    const PAGINATE_DEFAULT = 10;

    const FILTERED = [
        'id'         => "N°",
        'email'      => "Email",
        'system_id'  => "System",
        'created_at' => "Created",
        'updated_at' => "Updated",
    ];

    protected $fillname = ['email'];

    protected $fillable = [
        'email',
        'system_id',
    ];

    public function system()
    {
        return $this->belongsTo(System::class);
    }
}

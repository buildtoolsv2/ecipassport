<?php

namespace App\Models;

use App\Traits\FullSearch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    use HasFactory;
    use FullSearch;

    const PAGINATE_LIST = [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100];
    const PAGINATE_DEFAULT = 10;

    const FILTERED = [
        'id'         => "N°",
        'name'       => "Name",
        'domain'     => "Domain",
        'hash'       => "Hash",
        'created_at' => "Created",
        'updated_at' => "Updated",
    ];

    protected $fillname = ['name'];

    protected $fillable = [
        'name',
        'domain',
        'hash',
        'publickey',
    ];
}

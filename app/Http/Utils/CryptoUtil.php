<?php

namespace App\Utils;

use phpseclib\Crypt\RSA;

class CryptoUtil extends RSA
{
    public static function cuEncrypt($publicKey, $message)
    {
        $rsa = new RSA();
        try {
            $rsa->loadKey($publicKey); // public key
            $rsa->setEncryptionMode(RSA::ENCRYPTION_OAEP);
            return $rsa->encrypt($message);
        } catch (ErrorException $e) {
            return false;
        }
    }

    public static function cuDecrypt($privateKey, $message)
    {
        $rsa = new RSA();
        try {
            $rsa->loadKey($privateKey); // private key
            return $rsa->decrypt($message);
        } catch (ErrorException $e) {
            return false;
        }
    }
}

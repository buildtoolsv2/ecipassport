<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            "name"      => 'Name',
            "domain"    => 'Domain',
            "hash"      => 'Hash',
            "publickey" => 'Public Key',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"      => 'required|max:100',
            "domain"    => 'required|max:100',
            "hash"      => 'required|max:100',
            "publickey" => 'required|max:2000',
        ];
    }
}

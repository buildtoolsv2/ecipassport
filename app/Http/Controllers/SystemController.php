<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSystemRequest;
use App\Http\Requests\UpdateSystemRequest;
use App\Models\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $systems = System::orderBy('id', 'DESC');

        // Search
        $search = $request->get('search');
        $option = $request->get('option');

        $systems->filterSearchAll($search, System::FILTERED);

        // Paginate
        $paginate = $request->get('paginate') ?? System::PAGINATE_DEFAULT;

        $systems = $systems->paginate($paginate);

        return view('systems.index', compact('systems', 'search', 'paginate', 'option'));
        // systems/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('systems.create');
        // systems/create.blade.php
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSystemRequest $request)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $system = new System($request->validated());
        $system->save();
        return redirect(route('systems.index'))->with([
            'message' => 'The system was added correctly',
            'type' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function show(System $system)
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('systems.show', compact('system'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function edit(System $system)
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('systems.edit', compact('system'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSystemRequest $request, System $system)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $system->update($request->validated());
        return redirect(route('systems.index'))->with([
            'message' => 'The system was changed correctly',
            'type' => 'primary',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function destroyform(System $system)
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('systems.destroyform', compact('system'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function destroy(System $system)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $system->delete();
        return redirect(route('systems.index'))->with([
            'message' => 'The system was deleted correctly',
            'type' => 'danger',
        ]);
    }
}

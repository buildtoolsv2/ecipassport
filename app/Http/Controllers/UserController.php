<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $users = User::orderBy('id', 'DESC');

        // Search
        $search = $request->get('search');
        $option = $request->get('option');

        $users->filterSearchAll($search, User::FILTERED);

        // Paginate
        $paginate = $request->get('paginate') ?? User::PAGINATE_DEFAULT;

        $users = $users->paginate($paginate);

        return view('users.index', compact('users', 'search', 'paginate', 'option'));
        // users/index.blade.php
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('users.show', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroyform(User $user)
    {
        if (Auth::user()->superuser != 1) abort(404);

        return view('users.destroyform', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Auth::user()->superuser != 1) abort(404);

        $user->delete();
        return redirect(route('users.index'))->with([
            'message' => 'The user was deleted correctly',
            'type' => 'danger',
        ]);
    }
}

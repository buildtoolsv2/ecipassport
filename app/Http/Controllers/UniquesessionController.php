<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUniquesessionRequest;
use App\Http\Requests\UpdateUniquesessionRequest;
use App\Models\System;
use App\Models\Uniquesession;
use App\Models\User;
use App\Utils\CryptoUtil;
use Illuminate\Http\Request;
use phpseclib\Crypt\RSA;



class UniquesessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        $uniquesessions = Uniquesession::orderBy('id', 'DESC');

        // Search
        $search = $request->get('search');
        $option = $request->get('option');

        $uniquesessions->filterSearchAll($search, Uniquesession::FILTERED);

        // Paginate
        $paginate = $request->get('paginate') ?? Uniquesession::PAGINATE_DEFAULT;

        $uniquesessions = $uniquesessions->paginate($paginate);

        return view('uniquesessions.index', compact('uniquesessions', 'search', 'paginate', 'option'));
        // uniquesessions/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Auth::user()->superuser != 1) abort(404);

        return view('uniquesessions.create');
        // uniquesessions/create.blade.php
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUniquesessionRequest $request)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        $uniquesession = new Uniquesession($request->validated());
        $uniquesession->save();
        return redirect(route('uniquesessions.index'))->with([
            'message' => 'The session info was added correctly',
            'type' => 'success',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Uniquesession  $uniquesession
     * @return \Illuminate\Http\Response
     */
    public function show(Uniquesession $uniquesession)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        return view('uniquesessions.show', compact('uniquesession'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Uniquesession  $uniquesession
     * @return \Illuminate\Http\Response
     */
    public function edit(Uniquesession $uniquesession)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        return view('uniquesessions.edit', compact('uniquesession'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Uniquesession  $uniquesession
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUniquesessionRequest $request, Uniquesession $uniquesession)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        $uniquesession->update($request->validated());
        return redirect(route('uniquesessions.index'))->with([
            'message' => 'The session info was changed correctly',
            'type' => 'primary',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Uniquesession  $uniquesession
     * @return \Illuminate\Http\Response
     */
    public function destroyform(Uniquesession $uniquesession)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        return view('uniquesessions.destroyform', compact('uniquesession'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Uniquesession  $uniquesession
     * @return \Illuminate\Http\Response
     */
    public function destroy(Uniquesession $uniquesession)
    {
        if (\Auth::user()->superuser != 1) abort(404);

        $uniquesession->delete();
        return redirect(route('uniquesessions.index'))->with([
            'message' => 'The session info was deleted correctly',
            'type' => 'danger',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $hash = $request->get('hash');
        $referer = $request->headers->get('referer');
        $scheme = parse_url($referer, PHP_URL_SCHEME);
        $domain = parse_url($referer, PHP_URL_HOST);
        $system = System::where('hash', $hash)->first();
        if($system){
            $message = $request->get('message');
            $decrypted = CryptoUtil::cuDecrypt(config('pk.private_key'), $message);
            if($decrypted){
                $params = json_decode($decrypted, true);
                if(!isset($params['action'])){
                    $params['action'] = $request->get('action');
                }
                if($params['action'] == 'login'){
                    $user = User::where('email', $params['email'])->first();
                    if(!$user){
                        $user = User::create([
                            'name' => $params['email'],
                            'password' => $params['email'],
                            'email' => $params['email'],
                        ]); 
                    }
                    $user = User::where('email', $params['email'])->first();
                    \Auth::login($user);
                    $responseMessage = array(); 
                    $responseMessage['status'] = 200;
                    $responseMessage = CryptoUtil::cuEncrypt($system->publickey, json_encode($responseMessage));
                    return redirect($params['url'] . '?message=' . urlencode($responseMessage));
                }elseif($params['action'] == 'check'){
                    $responseMessage = array(); 
                    if (\Auth::check()) {
                        $responseMessage['status'] = 200;
                        $responseMessage['email'] = \Auth::user()->email;
                    }else{
                        $responseMessage['status'] = 401;
                    }
                    $responseMessage = CryptoUtil::cuEncrypt($system->publickey, json_encode($responseMessage));
                    return redirect()->intended($params['url'] . (strpos($params['url'], '?') === false ? '?': '&') .'message=' . urlencode($responseMessage));
                }else{
                    print "ERROR NO ACTION";
                    exit;
                    return redirect()->intended("$scheme://$domain" . '?status=' . 401);
                }
            }else{
                print "ERROR NO DEC";
                exit;
                return redirect()->intended("$scheme://$domain" . '?status=' . 401);
            }
        }else{
            print "ERROR NO SYS";
            exit;
            return redirect()->intended("$scheme://$domain" . '?status=' . 401);
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)//, $message)
    {
        \Auth::logout();
        \Session::flush();
        return redirect('/');
        $referer = $request->headers->get('referer');
        $scheme = parse_url($referer, PHP_URL_SCHEME);
        $domain = parse_url($referer, PHP_URL_HOST);
        $hash = $request->get('hash');
        $system = System::where('hash', $hash)->first();
        if($system){
            $message = urldecode($request->get('message'));
            $decrypted = CryptoUtil::cuDecrypt(config('pk.private_key'), $message);
            if($decrypted){
                $params = json_decode($decrypted);
                if($params['action'] == 'logout'){
                    \Auth::logout();
                    \Session::flush();
                    $responseMessage = array();
                    $responseMessage['status'] = 200;
                    $responseMessage = CryptoUtil::cuEncrypt($system->publickey, json_encode($message));
                    return redirect()->intended($params['url'] . '?message=' . urlencode($responseMessage));
                }else{
                    return redirect()->intended($referer . '?status=' . 401);
                }
            }
        }else{
            return redirect()->intended($referer . '?status=' . 401);
        }
    }
    
    public function check(Request $request){
        var_dump(\Auth::check());
    }
}

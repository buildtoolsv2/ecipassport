<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('uniquesessions/login', 'App\Http\Controllers\UniquesessionController@login');
Route::get('uniquesessions/logout', 'App\Http\Controllers\UniquesessionController@logout');
Route::get('uniquesessions/check', 'App\Http\Controllers\UniquesessionController@check');

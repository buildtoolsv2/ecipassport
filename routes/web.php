<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum'])->group(function(){
    Route::resource('systems', 'App\Http\Controllers\SystemController');
    Route::get(
        'systems/{system}/destroyform',
        [
            'as' => 'systems.destroyform',
            'uses' => 'App\Http\Controllers\SystemController@destroyform'
        ]
    );
    Route::resource('users', 'App\Http\Controllers\UserController');
    Route::get(
        'users/{user}/destroyform',
        [
            'as' => 'users.destroyform',
            'uses' => 'App\Http\Controllers\UserController@destroyform'
        ]
    );
});

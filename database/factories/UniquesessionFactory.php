<?php

namespace Database\Factories;

use App\Models\Uniquesession;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UniquesessionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Uniquesession::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

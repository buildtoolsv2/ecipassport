<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniquesessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uniquesessions', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->bigInteger('system_id')->unsigned();
            $table->foreign('system_id')
                ->references('id')
                ->on('systems')
                ->onDelete('no action')
                ->onUpdate('no action')
            ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uniquesessions');
    }
}

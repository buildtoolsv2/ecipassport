<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('systems.index') }}">Systems</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Create System</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br />
                    @endif
                    <form method="post" action="{{route('systems.store')}}">
                        @csrf
                        <div class="card">
                            <h5 class="card-header bg-success text-white">
                                <i class="fa fa-id-badge"></i>
                                Create System
                            </h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label for="name">Name:</label>
                                            <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label for="domain">Domain:</label>
                                            <input type="text" class="form-control" name="domain" value="{{old('domain')}}" placeholder="Domain" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label for="hash">Hash:</label>
                                            <input type="text" class="form-control" name="hash" value="{{old('hash')}}" placeholder="Hash" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
                                        <div class="form-group">
                                            <label for="publickey">Public Key:</label>
                                            <textarea class="form-control" name="publickey" placeholder="Public Key" style="height: 200px;">{{old('publickey')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-sm btn-success">
                                    Guardar
                                </button>
                                <a href="{{route('systems.index')}}" class="btn btn-sm btn-link">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Systems</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    @if(session()->get('message'))
                        <div class="col-12">
                            <div class="alert alert-{{ session()->get('type') ?? 'message' }}">{{session()->get('message')}}</div>
                        </div>
                    @endif
                    <div class="col-12">
                        <div class="card">
                            <h5 class="card-header bg-dark text-white">
                                <i class="fa fa-id-badge"></i>
                                Systems
                            </h5>
                            <div class="card-body">
                                <div class="col-12">
                                    {{ $systems->links() }}
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive">
                                        {{ Form::open(['url' => route('systems.index'), 'method' => 'get', 'id' => 'grid_filter_form']) }}
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        @foreach (App\Models\System::FILTERED as $key => $value)
                                                            <td>
                                                                <input
                                                                    @if (substr($key, -3, 3) == '_at')
                                                                        type="date"
                                                                        class="form-control form-control-sm datepicker"
                                                                    @else
                                                                        type="text"
                                                                        class="form-control form-control-sm"
                                                                    @endif
                                                                    name="search[{{ $key }}]"
                                                                    value="{{ isset($search[$key]) ? $search[$key] : '' }}"
                                                                    placeholder="{{ $value }}"
                                                                >
                                                            </td>
                                                        @endforeach
                                                        <td>
                                                            <button type="submit" class="btn btn-sm btn-success">Buscar</button>
                                                            <a href="{{ route('systems.index') }}" class="btn btn-sm btn-secondary">
                                                                Limpiar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>N°</th>
                                                        <th>Name</th>
                                                        <th>Domain</th>
                                                        <th>Hash</th>
                                                        <th>Created</th>
                                                        <th>Updated</th>
                                                        <th>
                                                            <a href="{{ route('systems.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Create</a>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($systems))
                                                        @foreach ($systems as $system)
                                                            <tr>
                                                                <th scope="row">{{ $system->id }}</th>
                                                                <td>{{ $system->name }}</td>
                                                                <td>{{ $system->domain }}</td>
                                                                <td>{{ $system->hash }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($system->created_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($system->created_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td>{{ \Carbon\Carbon::parse($system->updated_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($system->updated_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td style="white-space: nowrap;">
                                                                    <a href="{{ route('systems.show', $system->id) }}" class="btn btn-sm btn-secondary"><i class="fa fa-eye"></i> Show</a>
                                                                    <a href="{{ route('systems.edit', $system->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                                                                    <a href="{{ route('systems.destroyform', $system->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="100%">
                                                                Systems not found
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="100%">
                                                            {{ $systems->total() }} Items.
                                                        </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

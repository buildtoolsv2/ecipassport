<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('uniquesessions.index') }}">Uniquesessions</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ver Uniquesession</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <h5 class="card-header bg-secondary text-white">
                            <i class="fa fa-list"></i>
                            Ver Uniquesession
                        </h5>
                        <div class="card-body">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <input disabled type="text" class="form-control" value="{{ $uniquesession->email }}" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label>System:</label>
                                            <input disabled type="text" class="form-control" value="{{ $uniquesession->system->name }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label>Created:</label>
                                            <input disabled type="text" class="form-control" value="{{ \Carbon\Carbon::parse($uniquesession->created_at)->format('d/m/Y H:i:s') }}" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="form-group">
                                            <label>Updated:</label>
                                            <input disabled type="text" class="form-control" value="{{ \Carbon\Carbon::parse($uniquesession->updated_at)->format('d/m/Y H:i:s') }}" />
                                        </div>
                                    </div>
                                </div>
                                <a href="{{route('uniquesessions.index')}}" class="btn btn-sm btn-secondary">
                                    Volver
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

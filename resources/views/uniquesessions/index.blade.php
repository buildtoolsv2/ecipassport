<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Uniquesessions</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    @if(session()->get('message'))
                        <div class="col-12">
                            <div class="alert alert-{{ session()->get('type') ?? 'message' }}">{{session()->get('message')}}</div>
                        </div>
                    @endif
                    <div class="col-12">
                        <div class="card">
                            <h5 class="card-header bg-dark text-white">
                                <i class="fa fa-id-badge"></i>
                                Uniquesessions
                            </h5>
                            <div class="card-body">
                                <div class="col-12">
                                    {{ $uniquesessions->links() }}
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive">
                                        {{ Form::open(['url' => route('uniquesessions.index'), 'method' => 'get', 'id' => 'grid_filter_form']) }}
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        @foreach (App\Models\Uniquesession::FILTERED as $key => $value)
                                                            <td>
                                                                <input
                                                                    @if (substr($key, -3, 3) == '_at')
                                                                        type="date"
                                                                        class="form-control form-control-sm datepicker"
                                                                    @else
                                                                        type="text"
                                                                        class="form-control form-control-sm"
                                                                    @endif
                                                                    name="search[{{ $key }}]"
                                                                    value="{{ isset($search[$key]) ? $search[$key] : '' }}"
                                                                    placeholder="{{ $value }}"
                                                                >
                                                            </td>
                                                        @endforeach
                                                        <td>
                                                            <button type="submit" class="btn btn-sm btn-success">Buscar</button>
                                                            <a href="{{ route('uniquesessions.index') }}" class="btn btn-sm btn-secondary">
                                                                Limpiar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>N°</th>
                                                        <th>Email</th>
                                                        <th>System</th>
                                                        <th>Created</th>
                                                        <th>Updated</th>
                                                        <th>
                                                            <a href="{{ route('uniquesessions.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Agregar</a>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($uniquesessions))
                                                        @foreach ($uniquesessions as $uniquesession)
                                                            <tr>
                                                                <th scope="row">{{ $uniquesession->id }}</th>
                                                                <td>{{ $uniquesession->email }}</td>
                                                                <td>{{ $uniquesession->system->name }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($uniquesession->created_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($uniquesession->created_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td>{{ \Carbon\Carbon::parse($uniquesession->updated_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($uniquesession->updated_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td style="white-space: nowrap;">
                                                                    <a href="{{ route('uniquesessions.show', $uniquesession->id) }}" class="btn btn-sm btn-secondary"><i class="fa fa-eye"></i> Ver</a>
                                                                    <a href="{{ route('uniquesessions.edit', $uniquesession->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Editar</a>
                                                                    <a href="{{ route('uniquesessions.destroyform', $uniquesession->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Borrar</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="100%">
                                                                Uniquesessions not found
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="100%">
                                                            {{ $uniquesessions->total() }} Items.
                                                        </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Users</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    @if(session()->get('message'))
                        <div class="col-12">
                            <div class="alert alert-{{ session()->get('type') ?? 'message' }}">{{session()->get('message')}}</div>
                        </div>
                    @endif
                    <div class="col-12">
                        <div class="card">
                            <h5 class="card-header bg-dark text-white">
                                <i class="fa fa-id-badge"></i>
                                Users
                            </h5>
                            <div class="card-body">
                                <div class="col-12">
                                    {{ $users->links() }}
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive">
                                        {{ Form::open(['url' => route('users.index'), 'method' => 'get', 'id' => 'grid_filter_form']) }}
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        @foreach (App\Models\User::FILTERED as $key => $value)
                                                            <td>
                                                                <input
                                                                    @if (substr($key, -3, 3) == '_at')
                                                                        type="date"
                                                                        class="form-control form-control-sm datepicker"
                                                                    @else
                                                                        type="text"
                                                                        class="form-control form-control-sm"
                                                                    @endif
                                                                    name="search[{{ $key }}]"
                                                                    value="{{ isset($search[$key]) ? $search[$key] : '' }}"
                                                                    placeholder="{{ $value }}"
                                                                >
                                                            </td>
                                                        @endforeach
                                                        <td>
                                                            <button type="submit" class="btn btn-sm btn-success">Buscar</button>
                                                            <a href="{{ route('users.index') }}" class="btn btn-sm btn-secondary">
                                                                Limpiar
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>N°</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Admin</th>
                                                        <th>Created</th>
                                                        <th>Updated</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($users))
                                                        @foreach ($users as $user)
                                                            <tr>
                                                                <th scope="row">{{ $user->id }}</th>
                                                                <td>{{ $user->name }}</td>
                                                                <td>{{ $user->email }}</td>
                                                                <td>{{ $user->superuser == '1' ? 'Yes' : 'No' }}</td>
                                                                <td>{{ \Carbon\Carbon::parse($user->created_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($user->created_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td>{{ \Carbon\Carbon::parse($user->updated_at) ? Carbon\Carbon::parse(\Carbon\Carbon::parse($user->updated_at))->format('d/m/Y H:i').'hs' : ''}}</td>
                                                                <td style="white-space: nowrap;">
                                                                    <a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-secondary"><i class="fa fa-eye"></i> Show</a>
                                                                    @if ($user->superuser != 1)
                                                                        <a href="{{ route('users.destroyform', $user->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Logout</a>
                                                                    @else
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @endif
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="100%">
                                                                Users not found
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="100%">
                                                            {{ $users->total() }} Items.
                                                        </th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

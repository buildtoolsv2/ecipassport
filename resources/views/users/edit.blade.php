<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Lista de User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br />
                    @endif
                    <form method="post" action="{{route('users.update', $user->id)}}">
                        @csrf
                        @method('PATCH')
                        <div class="col-12">
                            <div class="card">
                                <h5 class="card-header bg-primary text-white">
                                    <i class="fa fa-id-badge"></i>
                                    Edit User
                                </h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                            <div class="form-group">
                                                <label for="name">Name:</label>
                                                <input type="text" class="form-control" name="name" value="{{old('name', $user->name)}}" />
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                            <div class="form-group">
                                                <label for="name">Domain:</label>
                                                <input type="text" class="form-control" name="domain" value="{{old('domain', $user->domain)}}" />
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                            <div class="form-group">
                                                <label for="name">Hash:</label>
                                                <input type="text" class="form-control" name="hash" value="{{old('hash', $user->hash)}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
                                            <div class="form-group">
                                                <label for="name">Public Key:</label>
                                                <textarea class="form-control" name="publickey" style="height: 200px;">{{old('publickey', $user->publickey)}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                            <div class="form-group">
                                                <label>Created:</label>
                                                <input disabled type="text" class="form-control" value="{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y H:i:s') }}" />
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                                            <div class="form-group">
                                                <label>Updated:</label>
                                                <input disabled type="text" class="form-control" value="{{ \Carbon\Carbon::parse($user->updated_at)->format('d/m/Y H:i:s') }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Guardar
                                    </button>
                                    <a href="{{route('users.index')}}" class="btn btn-sm btn-link">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
